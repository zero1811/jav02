package Java.TestCode;

import java.util.Scanner;

public class array {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số hàng của ma trận : ");
        int hang = scanner.nextInt();
        System.out.println("Nhập số cột của ma trận : ");
        int cot = scanner.nextInt();
        int [][] a = new int[hang][cot];
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                System.out.printf("A[%d][%d] : ",i,j);
                a[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                System.out.print(a[i][j] +" ");
            }
            System.out.println();
        }
    }
}
