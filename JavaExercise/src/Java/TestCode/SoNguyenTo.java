package Java.TestCode;

import java.util.Scanner;

public class SoNguyenTo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số n :");
        number = scanner.nextInt();
        for (int i=0;i<=number;i++){
            if (kiemTra(i)){
                System.out.print(i +" ");
            }
        }
    }
    public static boolean kiemTra(int n){
        if (n<2)
            return  false;
        else {
            int tmp = (int)Math.sqrt((double)n);
            for (int i=2;i<tmp;i++){
                if (n % i ==0)
                    return  false;
            }
            return true;
        }
    }
}
