package Java.TestCode;

import java.io.InputStream;
import java.util.Scanner;

public class InputConsole {
    public static void main(String[] args) {
        InputStream stream = System.in;
        Scanner scanner = new Scanner(stream);
        System.out.println("Name: ");
        String name = scanner.nextLine();
        System.out.println("Họ Tên: "+ name);
    }
}
