package Java.EX02.OOP;

public class Ex01HCN {
    public int chieuDai;
    public int chieuRong;

    //setter
    public void setChieuDai(int chieuDai) {
        this.chieuDai = chieuDai;
    }

    public void setChieuRong(int chieuRong) {
        this.chieuRong = chieuRong;
    }

    //getter
    public int getChieuDai() {
        return chieuDai;
    }

    public int getChieuRong() {
        return chieuRong;
    }

    //Tính chu vi
    public int chuVi(){
        return (this.chieuDai + this.chieuRong) * 2;
    }

    //Tính diện tích
    public int dienTich(){
        return this.chieuRong * this.chieuDai;
    }

    public String toString(){
        return "Chiều Dài : " +this.getChieuDai() +" Chiều Rộng : " +this.getChieuRong() +" Chu vi : " +this.chuVi() +" Diện Tích : " +this.dienTich();
    }
}
