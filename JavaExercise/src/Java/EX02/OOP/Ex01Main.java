package Java.EX02.OOP;

import java.util.Scanner;

public class Ex01Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Ex01HCN hinh01 = new Ex01HCN();
        int chieuDai,chieuRong;
        System.out.println("Chiều Dài : ");
        chieuDai = scanner.nextInt();
        System.out.println("Chiều Rộng : ");
        chieuRong = scanner.nextInt();
        hinh01.setChieuDai(chieuDai);
        hinh01.setChieuRong(chieuRong);
        hinh01.chuVi();
        hinh01.dienTich();
        System.out.println(hinh01.toString());
    }
}
