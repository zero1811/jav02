package Java.EX02.OOP;

public class Ex02SinhVien {
    private int maSinhVien;
    private String hoTen;
    private float diemLT;
    private float diemTH;
    //setter

    public void setMaSinhVien(int maSinhVien) {
        this.maSinhVien = maSinhVien;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setDiemTH(float diemTH) {
        this.diemTH = diemTH;
    }

    public void setDiemLT(float diemLT) {
        this.diemLT = diemLT;
    }

    //getter

    public int getMaSinhVien() {
        return maSinhVien;
    }

    public String getHoTen() {
        return hoTen;
    }

    public float getDiemLT() {
        return diemLT;
    }

    public float getDiemTH() {
        return diemTH;
    }
    //construction
    public float diemTB(){
        return getDiemLT() + getDiemTH();
    }
    public String toString(){
        return "Mã Sinh Viên : " +this.getMaSinhVien()
                +" Họ Tên : "+this.getHoTen()
                +" Điểm Lý Thuyết: "+this.getDiemLT()
                +" Điểm Thực Hành" +this.getDiemTH()
                +" Điểm Trung Bình : "+diemTB();
    }

}
