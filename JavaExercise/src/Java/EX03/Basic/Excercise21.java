package Java.EX03.Basic;

import sun.applet.Main;

import java.util.Scanner;

public class Excercise21 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        System.out.println("Nhập chuỗi cần tách : ");
        input = scanner.nextLine();
        String [] output = input.split(" ");
        for (String x : output) {
            System.out.println(x);
        }
    }
}
