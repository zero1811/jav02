package Java.EX03.Basic;

import java.util.Scanner;

public class Excercise19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        System.out.println("Nhập chuỗi cần kiểm tra : ");
        input = scanner.nextLine();
        int dem = 0;
        char tmp;
        for (int i=0;i<input.length();i++){
            tmp = input.charAt(i);
            if (Character.toString(tmp).equals("a")){
                dem ++;
            }
        }
        System.out.println("Số lần xuất hiện của ký tự a là : "+dem);
    }
}
