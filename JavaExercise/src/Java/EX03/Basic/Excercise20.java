package Java.EX03.Basic;

import java.util.Scanner;

public class Excercise20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input;
        System.out.println("Nhập chuỗi cần kiểm tra : ");
        input = scanner.nextLine();
        int dem = 0;
        for (int i = 0 ; i < input.length() ; i++){
            char tmp = input.charAt(i);
            if (Character.isDigit(tmp)){
                dem ++ ;
            }
        }
        System.out.println("Số ký tự là số là : "+dem);
    }
}
