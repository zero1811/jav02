package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise04 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số cần phân tích : ");
        number = scanner.nextInt();
        int dem =0;
        for (int i=2;i<=number;i++){
            while (number % i ==0){
                dem++;
                number/=i;
            }
            if (dem>1)
                System.out.printf("%d^%d",i,dem);
            else
                System.out.printf("%d",i);
            if (number>i)
                System.out.print(" * ");
        }
    }
}
