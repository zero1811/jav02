package Java.EX01.Basic;

public class Excercise26 {
    public static void main(String[] args) {
        final  int MIN = 1000000;
        final  int MAX = 9999999;
        for (int i = MIN ; i <= MAX ; i++)
            if (nguyenTo(i) && thuanNghich(i) && thuanNghich(tongCacChuSo(i)))
                System.out.print(i + " ");

    }
    public static boolean nguyenTo(int n){
        if (n < 2)
            return false;
        else {
            for (int i=2 ; i < n-1 ; i++){
                if (n % i == 0)
                    return false;
            }
            return true;
        }
    }
    public static  int daoNguoc(int n){
        int sum = 0;
        while (n % 10 != 0 || n / 10 != 0){
            sum = (sum * 10) + n % 10;
            n/=10;
        }
        return  sum;
    }
    public static boolean thuanNghich(int n){
        if (daoNguoc(n) == n)
            return true;
        else
            return false;
    }
    public static int tongCacChuSo(int n){
        int sum = 0;
        while (n / 10 != 0 || n % 10 != 0){
            sum += n % 10;
            n/=10;
        }
        return sum;
    }
}
