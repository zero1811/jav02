package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise23 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số nguyên n : ");
        number =scanner.nextInt();
        System.out.println(number+ " Số nguyên tố đầu tiên là: ");
        for (int i = 0; i < number; i++) {
            if (nguyenTo(i))
                System.out.print(i +" ");
        }
        System.out.println();
        System.out.println(number +" Số fibonaci đầu tiên là : ");
        for (int i = 0; i < number; i++) {
            System.out.print(fibonaxi(i)+ " ");
        }
    }
    public static boolean nguyenTo(int n){
        if (n<2)
            return false;
        else {
            for (int i = 2 ; i < n-1 ; i++)
                if (n % i == 0)
                    return false;
            return  true;
        }
    }
    public static int fibonaxi(int n){
        if (n == 1 || n ==0)
            return 1;
        else
            return fibonaxi(n-1) + fibonaxi(n-2);
    }
}
