package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise08 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số cần kiểm tra: ");
        number = scanner.nextInt();
        int revNumber = 0;
        int tmpNumber = number;
        while (tmpNumber / 10 != 0 || tmpNumber % 10 != 0){
            revNumber = (revNumber * 10) + (tmpNumber %10);
            tmpNumber/=10;
        }
        if (revNumber == number)
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}
