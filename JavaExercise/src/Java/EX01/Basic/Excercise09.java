package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise09 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size;
        System.out.println("Nhập độ dài của xâu nhị phân : ");
        size = scanner.nextInt();
        int a[] =new int[size];
        for (int i=0;i<size;i++)
            a[i] = 0;
        for (int i=0;i<Math.pow(size,2);i++){
            xuat(a,size);
            tao(a,size);
        }
    }
    public static void tao(int a[],int n){
        ++a[n-1];
        for (int i=n-1;i>=0;i--){
            if (a[i] >1){
                ++a[i-1];
                a[i]-=2;
            }
        }
    }
    public static void xuat(int a[],int n){
        for (int i=0;i<n;i++)
            System.out.print(a[i]);
        System.out.println();
    }
}
