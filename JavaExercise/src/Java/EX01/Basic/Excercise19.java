package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int S;
        System.out.println("Nhập tổng S : ");
        S = scanner.nextInt();
        for (int i = 10000 ; i <= 99999 ; i++){
            if (nguyenTo(i))
                if (tinhTong(i) == S)
                    System.out.println(i);
        }
    }
    public static boolean nguyenTo(int n){
        if (n<2)
            return false;
        else {
            for (int i = 2 ; i < n-1 ; i++)
                if (n % i == 0)
                    return false;
                return  true;
        }
    }
    public static int tinhTong(int n){
        int sum =0;
        while (n%10!=0 || n/10!=0){
            sum += n%10;
            n/=10;
        }
        return sum;
    }
}
