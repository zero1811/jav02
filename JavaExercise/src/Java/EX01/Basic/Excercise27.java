package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise27 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        final  int MAX_SIZE = 100;
        int [] a = new int[MAX_SIZE];
        System.out.println("Nhập kích thước mảng : ");
        int size = scanner.nextInt();
        for (int i=0 ; i<size ; i++){
            System.out.printf("a[%d] : ",i);
            a[i] = scanner.nextInt();
        }
        System.out.println();
        sapXep(a,size);
        System.out.println("Mảng sau khi được sắp xếp là : ");
        xuat(a,size);
        System.out.println();
        timMax(a,size);
        System.out.println();
        chenX(a,size);

    }
    public static void sapXep(int a[],int n){
        for (int i = 0 ; i < n ; i++){
            for (int j = i+1  ; j < n ; j++){
                if (a[i] < a[j]){
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }
    }
    public static void  timMax(int a[],int n){
        sapXep(a,n);
        System.out.println("Giá trị lớn thứ 2 của mảng là : "+a[1]);
    }
    public static void chenX(int a[], int n) {
        System.out.printf("Nhập số muốn chèn : ");
        Scanner scanner = new Scanner(System.in);
        int X = scanner.nextInt();
        int viTri = 0;
        for (int i = 0; i < n; i++) {
            if (X < a[i] && X > a[i + 1])
                viTri = i + 1;
        }
        System.out.println(viTri);
        for (int i = n; i > viTri; i--)
            a[i] = a[i - 1];
        a[viTri] = X;
        n += 1;
        System.out.println("Mảng sau khi trèn " + X + "là : ");
        xuat(a,n);
    }
    public static void xuat(int a[],int n){
        for (int i = 0 ; i < n ; i++)
            System.out.print(a[i] +" ");
    }
}
