package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise22 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số nguyên n : ");
        number = scanner.nextInt();
        System.out.println("Các ước của "+number+" là : ");
        for (int i=1;i<=number;i++){
            if (number % i == 0)
                System.out.print(i +" ");
        }
        System.out.println();
        System.out.println("Các ước của "+number+" là số nguyên tố là : ");
        for (int i = 1; i < number; i++) {
            if (number % i == 0)
                if (nguyenTo(i))
                    System.out.print(i+ " ");
        }
    }
    public static boolean nguyenTo(int n){
        if (n<2)
            return false;
        else {
            for (int i = 2 ; i < n-1 ; i++)
                if (n % i == 0)
                    return false;
            return  true;
        }
    }
}
