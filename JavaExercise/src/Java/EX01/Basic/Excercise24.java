package Java.EX01.Basic;

import java.util.Scanner;

//Fail
public class Excercise24 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int hang,cot;
        System.out.println("Nhập số hàng của ma trận : ");
        hang = scanner.nextInt();
        System.out.println("Nhập số cột của ma trận : ");
        cot = scanner.nextInt();
        int [][] a = new int[hang][cot];
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                System.out.printf("A[%d][%d] : ",i,j);
                a[i][j] = scanner.nextInt();
            }
        }
        System.out.println();
        System.out.println("Mảng vừa nhập là : ");
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                System.out.print(a[i][j] +" ");
            }
            System.out.println();
        }
        System.out.println();
        phanTuLonNhat(a,hang,cot);
        sapXep(a,hang,cot);
        System.out.println();
        phanTuLaNguyenTo(a,hang,cot);
    }
    public static void phanTuLonNhat(int a[][],int m,int n){
        int max = a[0][0];
        for (int i = 0; i <m ; i++) {
            for (int j = 0; j < n; j++) {
                if (max < a[i][j])
                    max = a[i][j];
            }
        }
        System.out.println("Phần tử lớn nhất là: "+max);
    }
    public static void phanTuLaNguyenTo(int a[][],int m,int n){
        System.out.println("Các phần tử là số nguyên tố của mảng là : ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (!nguyenTo(a[i][j]))
                    a[i][j] = 0;
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j]+ " ");
            }
            System.out.println();
        }
    }
    public static boolean nguyenTo(int n){
        if (n<2)
            return false;
        else {
            for (int i = 2 ; i < n-1 ; i++)
                if (n % i == 0)
                    return false;
            return  true;
        }
    }
    public static void sapXep(int a[][],int m,int n){

        System.out.println("Mảng đã được sắp xếp : ");
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] +" ");
            }
            System.out.println();
        }
    }
}
