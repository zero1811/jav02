package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise06 {
    public static boolean check(int n){
        int k = (int)Math.sqrt((double)n);
        if (n < 2)
            return  false;
        else{
            for (int i=2;i<n;i++){
                if (n % i == 0){
                    return false;
                }
            }
            return true;
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhâp số n: ");
        number = scanner.nextInt();
            for (int i= 0 ; i<= number ; i++){
                if (check(i) == true)
                    System.out.print(i +" ");
            }
    }

}
