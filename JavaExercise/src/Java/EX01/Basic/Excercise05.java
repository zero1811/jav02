package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số n : ");
        number = scanner.nextInt();
        for (int i =0;i<number;i++){
            if (kiemTra(i)){
                System.out.print(i +" ");
            }
        }
    }
    public static boolean kiemTra(int n){
        if (n < 2)
            return false;
        else {
            for (int i=2;i<n-1;i++){
                if (n % i ==0)
                    return false;
            }
            return true;
        }
    }
}
