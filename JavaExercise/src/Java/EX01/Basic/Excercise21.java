package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise21 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số nguyên n : ");
        number =scanner.nextInt();
        System.out.println("Tổng các chữ số của n là : " +tinhTong(number));
        System.out.println("Phân tích n thành các thừa số nguyên tố : ");
        phanTich(number);

    }
    public static int tinhTong(int n){
        int sum =0;
        while (n%10!=0 || n/10!=0){
            sum += n%10;
            n/=10;
        }
        return sum;
    }
    public static void phanTich(int n){
        int dem=0;
        for (int i=2;i<=n;i++){
            while (n%i==0){
                dem++;
                n/=i;
            }
            if (dem > 1)
                System.out.printf("%d^%d",i,dem);
            else
                System.out.printf("%d",i);
            if (n > i)
                System.out.print(" * ");
        }
    }
}
