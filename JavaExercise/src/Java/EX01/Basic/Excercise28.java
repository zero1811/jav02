package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise28 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số hàng của ma trận :  ");
        int hang = scanner.nextInt();
        System.out.println("Nhập số cột của ma trận : ");
        int cot = scanner.nextInt();
        int [][] a =  new int[hang][cot];
        nhap(a,hang,cot);
        System.out.println("Mảng vừa nhập là : ");
        xuat(a,hang,cot);
        System.out.println();
        timMax(a,hang,cot);
        mangNguyenTo(a,hang,cot);
        System.out.println();
        hangNguyenTo(a,hang,cot);

    }
    public static void nhap(int a[][],int hang,int cot){
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                System.out.printf("A[%d][%d] : ",i,j);
                a[i][j] = scanner.nextInt();
            }
        }
    }
    public static void xuat(int a[][],int hang,int cot){
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void timMax(int a[][],int hang,int cot){
        int max = a[0][0];
        int viTriHang,viTriCot;
        viTriHang = viTriCot = 0;
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                if (max < a[i][j]){
                    max = a[i][j];
                    viTriHang = i;
                    viTriCot = j;
                }
            }
        }
        System.out.println("Phần tử lớn nhất là: "+max +" Vị trí hàng : "+viTriHang +" Vị trí cột : "+viTriCot);
    }
    public static boolean laNguyenTo (int n){
        if (n < 2){
            return false;
        }
        else {
            for (int i = 2; i < n-1; i++) {
                if ( n % i == 0)
                    return false;
            }
            return true;
        }
    }
    public static void mangNguyenTo (int a[][],int hang,int cot){
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                if (!laNguyenTo(a[i][j])==false)
                    a[i][i] = 0;
            }
        }
        System.out.println("Mảng các số nguyên tố là : ");
        xuat(a,hang,cot);
    }
    public static void hangNguyenTo (int a[],int hang,int cot){
        int demNguyenTo[] = new int[hang];
        int demHang = 0;
        for (int i = 0; i < hang; i++) {
            demHang[i] = 0;
        }
        for (int i = 0; i < hang; i++) {
            for (int j = 0; j < cot; j++) {
                if (laNguyenTo(a[i][j]))
                    demNguyenTo[demHang] ++;
            }
        }
    }
}