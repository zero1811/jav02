package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number,Sum=0;
        System.out.println("Nhập số n: ");
        number = scanner.nextInt();
        while (number%10!=0 || number/10!=0){
            Sum+=number%10;
            number/=10;
        }
        System.out.println("Tổng các chữ số của n là : " + Sum);
    }
}
