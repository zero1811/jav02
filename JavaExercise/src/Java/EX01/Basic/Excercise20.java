package Java.EX01.Basic;

import java.util.Scanner;

public class Excercise20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number;
        System.out.println("Nhập số n : ");
        number =scanner.nextInt();
        int i =0;
        while (fibonaxi(i) < number){
            if (nguyenTo(fibonaxi(i)))
                System.out.println(fibonaxi(i));
            i++;
        }
    }
    public static boolean nguyenTo(int n){
        if (n<2)
            return false;
        else {
            for (int i = 2 ; i < n-1 ; i++)
                if (n % i == 0)
                    return false;
            return  true;
        }
    }
    public static int fibonaxi(int n){
        if (n == 1 || n ==0)
            return 1;
        else
            return fibonaxi(n-1) + fibonaxi(n-2);
    }
}
