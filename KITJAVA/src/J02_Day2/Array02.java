package J02_Day2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Array02 {
    public static void main(String[] args) {
        int[] array = new int[5];
        Scanner scanner = new Scanner(System.in);
        //Nhập mảng
        for (int i = 0; i < 5; i++) {
            System.out.print("Array[" + i + "] : ");
            array[i] = scanner.nextInt();
        }
        //Sắp xếp mảng
        Arrays.sort(array);
        //in mảng
        System.out.println("Mảng vừa nhập : ");
        for (int i = 0; i < 5; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        //Tìm số nguyên k thuộc mảng
        System.out.println("Nhập số nguyên k : ");
        int k = scanner.nextInt();
        if (Arrays.binarySearch(array,k) > 0)
            System.out.println("Có");
        else
            System.out.println("Không");
        /*
        for (int i = 0; i < array.length; i++) {
            if (array[i] == k){
                System.out.println("Có");
                break;
            }
            if (i == array.length -1)
                System.out.println("Không");
        }
         */
    }
}