package J02_Day2;

import java.util.Scanner;

public class Array01 {
    public static void main(String[] args) {
        //Ví dụ mảng 1 chiều
        int n = 10;
        int[] array = new int[n];
        Scanner scanner = new Scanner(System.in);
        //Nhập mảng
        for (int i = 0; i < n; i++) {
            System.out.print("Array["+i+"] :");
            array[i] = scanner.nextInt();
        }
        System.out.println();
        //Xuất mảng
        for (int i = 0; i < array.length; i++) {
            System.out.println("array["+i+"] = "+array[i]);
        }
    }
}
