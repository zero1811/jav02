package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise13 {
    public static void main(String[] args) {
        System.out.println("Nhập chuỗi : ");
        Scanner scanner = new Scanner(System.in);
        String str01 = scanner.nextLine();
        System.out.println("Nhập ký tự cần tìm : ");
        String chr = scanner.next();
        int coutChar = 0;
        for (int i = 0; i < str01.length(); i++) {
            if (chr.equalsIgnoreCase(String.valueOf(str01.charAt(i))))
                coutChar ++;
        }
        if (coutChar > 0)
            System.out.println("Chuỗi : "+str01+" Có "+coutChar+" ký tự "+chr);
    }
}
