package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số hàng : ");
        int row = scanner.nextInt();
        System.out.println("Nhập số cột : ");
        int col = scanner.nextInt();
        int [][] Arr01 = new int[row][col];
        int [][] Arr02 = new int[row][col];
        int [][] result = new int[row][col];
        // Nhập ma trận 1
        System.out.println("Nhập ma trận 1 : ");
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.printf("Array01[%d][%d] : ",i,j);
                Arr01[i][j] = scanner.nextInt();
            }
        }
        //Nhập ma trận 2
        System.out.println("Nhập ma trận 2 : ");
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.printf("Array02[%d][%d] : ",i,j);
                Arr02[i][j] = scanner.nextInt();
            }
        }
        //Nhân hai ma trận
        for (int i = 0; i < col; i++) {
            for (int j = 0; j < row; j++) {
                for (int k = 0; k < col; k++) {
                    result[i][j] += Arr01[i][j] * Arr02[k][j];
                }
            }
        }
        //in kết quả
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.print(result[i][j] +" ");
            }
            System.out.println();
        }
    }
}
