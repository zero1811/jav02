package J02_Day2_Excercise;


import java.util.Scanner;

public class Excercise03 {
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int array [] = new int[size];
        //Nhập mảng
        for (int i = 0; i < size; i++) {
            System.out.printf("Array[%d] : ",i);
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i < array.length; i++) {
            if (isPrime(array[i]))
                System.out.print(array[i] +" ");
        }
    }
    public static boolean isPrime (int number){
        if (number < 2)
            return  false;
        else {
            for (int i = 2; i < number -1; i++) {
                if (number % i == 0)
                    return false;
            }
            return true;
        }
    }
}
