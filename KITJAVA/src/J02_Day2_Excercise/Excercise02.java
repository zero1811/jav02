package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise02 {
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int [] arrayInt = new int[size];
        // Nhập mảng
        for (int i = 0; i < size; i++) {
            System.out.println("Array ["+i+"] : ");
            arrayInt[i] = scanner.nextInt();
        }
        int sum = 0 ;//Tổng
        for (int i = 0; i < arrayInt.length; i++) {
            sum+=arrayInt[i];
        }
        System.out.println("Tổng các phần tử trong mảng là : "+sum);
    }
}
