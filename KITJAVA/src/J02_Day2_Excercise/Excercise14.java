package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise14 {
    public static void main(String[] args) {
        System.out.println("Nhập chuỗi 1 : ");
        Scanner scanner = new Scanner(System.in);
        String str01 = scanner.nextLine();
        System.out.println("Nhập chuỗi 2 : ");
        String str02 = scanner.nextLine();
        String result = str01 + str02;
        System.out.println("Chuỗi vừa nối : "+result);
    }
}
