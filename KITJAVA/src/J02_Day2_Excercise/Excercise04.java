package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise04 {
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int [] arrayInt = new int[size];
        // Nhập mảng
        for (int i = 0; i < size; i++) {
            System.out.println("Array [" +i+"] : ");
            arrayInt[i] = scanner.nextInt();
        }
        System.out.println("Nhập số cần tìm : ");
        int number = scanner.nextInt();
        int countNumber = 0;
        for (int i = 0; i < arrayInt.length; i++) {
            if (arrayInt[i] == number)
                countNumber ++ ;
        }
        if (countNumber > 0){
            System.out.println("Số "+number+" xuất hiện "+countNumber+" lần trong mảng");
        }
        else
            System.out.println("Số "+number+" không có trong mảng");
    }
}
