package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise15 {
    public static void menu(){
        System.out.println("1. In đâm chuỗi");
        System.out.println("2. In thường chuỗi");
        System.out.println("3. Thoát");
    }
    public static void main(String[] args) {
        System.out.println("Nhập chuỗi : ");
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        while (true){
            menu();
            int choose = scanner.nextInt();
            switch (choose){
                case 1:{
                    System.out.println("Chuỗi in đậm : ");
                    System.out.println(str.toUpperCase());
                    break;
                }
                case 2:{
                    System.out.println("Chuỗi in thường : ");
                    System.out.println(str.toLowerCase());
                    break;
                }
                case 3:
                    return;
            }
        }
    }
}
