package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise09 {
    public static void main(String[] args) {
        System.out.println("Nhập số hàng của ma trận : ");
        Scanner scanner = new Scanner(System.in);
        int row = scanner.nextInt();
        System.out.println("Nhập số cột của ma trận : ");
        int col = scanner.nextInt();
        int [][] Arr = new int[row][col];
        //Nhập ma trận
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                System.out.printf("Array[%d][%d] : ",i,j);
                Arr[i][j] = scanner.nextInt();
            }
        }
        for (int i = 0; i <Arr.length ; i++) {
            for (int j = 0; j < Arr[i].length; j++) {
                if (!isPrime(Arr[i][j]))
                    Arr[i][j] = 0;
            }
        }
        for (int i = 0; i < Arr.length; i++) {
            for (int j = 0; j < Arr[i].length; j++) {
                System.out.print(Arr[i][j]+" ");
            }
            System.out.println();
        }
    }
    public static boolean isPrime (int number){
        if (number < 2)
            return  false;
        else {
            for (int i = 2; i < number -1; i++) {
                if (number % i == 0)
                    return false;
            }
            return true;
        }
    }
}
