package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise07 {
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int [] array = new int[size];
        //Nhập mảng
        for (int i = 0; i < size; i++) {
            System.out.println("array["+i+"] : ");
            array[i] = scanner.nextInt();
        }
        int max = array[0];
        int min = array[0];
        int locationMax = 0;
        int locationMin = 0;
        //Tìm max
        for (int i = 0; i < array.length; i++) {
            if (max < array[i]){
                max = array[i];
                locationMax = i;
            }
        }
        //Tìm min
        for (int i = 0; i < array.length; i++) {
            if (min > array[i]){
                min = array[i];
                locationMin = i;
            }
        }
        System.out.printf("Giá trị lớn nhất là %d nằm ở vị trí %d \n",max,locationMax);
        System.out.printf("Giá trị nhỏ nhất là %d nằm ở vị trí %d ",min,locationMin);
    }
}
