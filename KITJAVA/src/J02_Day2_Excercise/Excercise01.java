package J02_Day2_Excercise;

import java.util.Arrays;
import java.util.Scanner;

public class Excercise01 {
    public static void menu(){
        System.out.println("Mời bạn chọn : ");
        System.out.println("1. Sắp xếp tăng dần");
        System.out.println("2. Sắp xếp giảm dần");
        System.out.println("3. Thoát");
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập kích thước mảng : ");
        int size = scanner.nextInt();
        int [] array = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Array[" +i +"] : ");
            array[i] = scanner.nextInt();
        }
        while (true) {
            menu();
            int choose = scanner.nextInt();
            switch (choose) {
                case 1: {
                    Arrays.sort(array);
                    System.out.println("Mảng đã được sắp xếp tăng dần : ");
                    for (int i = 0; i < array.length; i++) {
                        System.out.println(array[i]);
                    }
                    break;
                }
                case 2: {
                    System.out.println("Mảng sau khi sắp xếp giảm : ");
                    sortDown(array, size);
                    for (int i = 0; i < array.length; i++) {
                        System.out.println(array[i]);
                    }
                    break;
                }
                case 3:return;
                default:{
                    System.out.println("Chọn lại");
                    break;
                }
            }
        }
    }
    public static void sortDown (int a[],int size){
        for (int i = 0; i < size-1; i++) {
            for (int j = i+1; j < size; j++) {
                if (a[i] < a[j] ){
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }

            }
        }
    }
}
