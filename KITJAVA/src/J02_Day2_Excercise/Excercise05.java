package J02_Day2_Excercise;

import java.util.Scanner;

public class Excercise05 {
    public static void main(String[] args) {
        System.out.println("Nhập kích thước mảng : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int [] array = new int[size];
        // Nhập mảng
        for (int i = 0; i < size; i++) {
            System.out.println("Array ["+i+"] : ");
            array[i] = scanner.nextInt();
        }
        System.out.println("Nhập vị trí cần hiện : ");
        int location = scanner.nextInt();
        for (int i = 0; i < array.length; i++) {
            if (i == location -1)
                System.out.println(array[i]);
        }
    }
}
