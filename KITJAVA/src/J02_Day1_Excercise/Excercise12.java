package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 3; i++) {
            System.out.println("Nhập số thực : ");
            double number = scanner.nextDouble();
            if (number < 0)
                number = number * -1.0;
            if (number == 0)
                number = 0;
            System.out.println(number);
        }
    }
}
