package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise14 {
    public static void main(String[] args) {
        System.out.println("Nhập kích thước hình : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (j == 0 || i >= j || i == size-1)
                    System.out.print(" * ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }
    }
}
