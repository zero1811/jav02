package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise05 {
    public static void main(String[] args) {
        System.out.println("Nhập số nguyên n : ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (isEvenNumber(n))
            System.out.println(" Toàn số chẵn");
        else
            if (isOldNumber(n))
                System.out.println("Toàn số lẻ");
            else
                System.out.println("Không toàn số lẻ cũng không toàn số chẵn");
    }
    public static boolean isEvenNumber(int number){
        int countEven = 0;
        int count = 0;
        while (number % 10 != 0 || number / 10 != 0){
            int tmp = number % 10;
            if (tmp % 2 == 0)
                countEven ++;
            number /= 10;
            count ++ ;
        }
        if (count == countEven)
            return true;
        else
            return false;
    }
    public static boolean isOldNumber(int number){
        int countOld = 0;
        int count = 0;
        while (number % 10 != 0 || number / 10 != 0){
            int tmp = number % 10;
            if (tmp % 2 != 0)
                countOld ++;
            number /= 10;
            count ++ ;
        }
        if (count == countOld)
            return true;
        else
            return false;
    }
}
