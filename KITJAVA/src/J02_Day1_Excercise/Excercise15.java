package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise15 {
    public static void main(String[] args) {
        System.out.println("Nhập kích thước của hình : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int k = size -1;
        for (int i = 0; i < size -1; i++) {
            for (int j = 0; j < 2*size -1; j++) {
                if (j == k-i || j == k+i)
                    System.out.print(" * ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }
        for (int i = 0; i < 2*size-1; i++) {
            System.out.print(" * ");
        }
    }
}
