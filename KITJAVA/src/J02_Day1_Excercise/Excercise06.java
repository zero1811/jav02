package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise06 {
    public static void main(String[] args) {
        System.out.println("Nhập số nguyên dương n : ");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int countNumber = 0; // Đếm các chữ số của n nếu n giảm dần từ phải sang
        int count = 0; //Đếm các chữ số của n
        while (number / 10 != 0 || number % 10 != 0){
            int tmp = number % 10 ;
            number /= 10;
            if (tmp > number % 10)
                countNumber ++ ;
            count ++;
        }
        if (count == countNumber)
            System.out.println("Tăng dần trái sang phải");
        else
            System.out.println("Không tăng dần từ trái sang phải");
    }
}
