package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số nguyên dương n : ");
        int n = scanner.nextInt();
        System.out.println("Số vừa nhập là : " +n);
        int max = 0;
        while (n / 10 != 0 || n % 10 != 0){
            int tmp = n % 10 ;
            if (tmp > max)
                max = tmp;
            n/=10;
        }
        System.out.println("Số lớn nhất là : " +max);
    }
}
