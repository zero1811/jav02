package J02_Day1_Excercise;

public class Excercise08 {
    public static void main(String[] args) {
        final  int MAXSUM = 10000;
        int sum = 0;
        int n;
        for (n = 0; ; n++) {
            sum+=n;
            if (sum > MAXSUM)
                break;
        }
        System.out.println("Số n nhỏ nhất là : " +n);
    }
}
