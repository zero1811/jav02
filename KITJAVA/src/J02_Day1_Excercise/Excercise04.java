package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise04 {
    public static void main(String[] args) {
        System.out.println("Nhập số nguyên dương n : ");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int min = isMin(number);
        int countMin = 0;
        while (number / 10 != 0 || number % 10 != 0){
            int tmp = number % 10;
            System.out.println(tmp);
            if (min == tmp)
                countMin ++;
            number /= 10;
        }
        System.out.println("Có "+countMin +" số nhỏ nhất trong n");
    }
    public static int isMin(int number){
        int min = number % 10 ;
        while (number / 10 != 0 || number % 10 != 0){
            int tmp = number % 10;
            if (min > tmp)
                min = tmp;
            number /= 10;
        }
        return min;
    }
}
