package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số nguyên n : ");
        int n = scanner.nextInt();
        System.out.println("n = " +n);
        int result = 0; // Tổng
        while (n/10!=0 || n%10!=0){
            int tmp = n%10;
            if (tmp % 2 == 0)
                result+=tmp;
            n/=10;
        }
        System.out.println("Tổng các chữ số chắn bằng : "+result);

    }
}
