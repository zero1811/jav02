package J02_Day1_Excercise;

import java.util.Scanner;

public class Excercise10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số cần kiểm tra : ");
        int number = scanner.nextInt();
        System.out.println("Số vừa nhập là : "+number);
        if (isPrime(number) == true)
            System.out.println("Số vừa nhập là số nguyên tố");
        else
            System.out.println("Số vừa nhập không là số nguyên tố");
    }
    public static boolean isPrime(int number){
        if (number < 2)
            return  false;
        else {
            //int tmp = (int)Math.sqrt((double)number);
            for (int i = 2; i < number; i++){
                if (number % i == 0){
                    return false;
                }
            }
            return true;
        }
    }
}
