package J02_Day3.Excercise02;

import java.util.Calendar;

public class Teacher {
    private String teacherName;
    private int teacherId;
    private int teacherNamSinh;
    private int age;

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getTeacherNamSinh() {
        return teacherNamSinh;
    }

    public void setTeacherNamSinh(int teacherNamSinh) {
        this.teacherNamSinh = teacherNamSinh;
    }

    public int getAge() {
        int age = 0;
        Calendar now = Calendar.getInstance();
        age = now.get(Calendar.YEAR) - this.getTeacherNamSinh();
        return age;
    }
    public void sowInfo (){
        System.out.println("Id : "+this.getTeacherId());
        System.out.println("Họ Tên : "+this.getTeacherName());
        System.out.println("Năm sinh : "+this.getTeacherNamSinh());
        System.out.println("Tuổi : "+this.getAge());
    }
}
