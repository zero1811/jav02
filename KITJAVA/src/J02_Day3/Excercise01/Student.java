package J02_Day3.Excercise01;

import java.util.Calendar;

public class Student {
    private int sutdent_Id;
    private String student_FirstName;
    private String student_LastName;
    private int sutdent_BirDay;
    private int age;
    //Setter and getter

    public int getSutdent_Id() {
        return sutdent_Id;
    }

    public void setSutdent_Id(int sutdent_Id) {
        this.sutdent_Id = sutdent_Id;
    }

    public String getStudent_FirstName() {
        return student_FirstName;
    }

    public void setStudent_FirstName(String student_FirstName) {
        this.student_FirstName = student_FirstName;
    }

    public String getStudent_LastName() {
        return student_LastName;
    }

    public void setStudent_LastName(String student_LastName) {
        this.student_LastName = student_LastName;
    }

    public int getSutdent_BirDay() {
        return sutdent_BirDay;
    }

    public void setSutdent_BirDay(int sutdent_BirDay) {
        this.sutdent_BirDay = sutdent_BirDay;
    }

    public int getAge() {
        int age  = 0;
        Calendar now = Calendar.getInstance();
        age = now.get(Calendar.YEAR) - this.getSutdent_BirDay();
        return age;
    }
    public void showInfo(){
        System.out.println("Id : "+this.getSutdent_Id());
        System.out.println("Họ Tên : "+this.getStudent_FirstName() +" "+this.getStudent_LastName());
        System.out.println("Ngày sinh : "+this.getSutdent_BirDay());
        System.out.println("Tuổi : "+this.getAge());
    }

    public void setInput() {
    }
}
