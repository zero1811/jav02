package J02_Day3_Excercise;

import java.util.Scanner;

public class Member04 {
    String memberCode;
    String memberName;
    String memberSex;
    int memberBirthDay;
    String memberTeam;
    String position;

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberSex() {
        return memberSex;
    }

    public void setMemberSex(String memberSex) {
        this.memberSex = memberSex;
    }

    public int getMemberBirthDay() {
        return memberBirthDay;
    }

    public void setMemberBirthDay(int memberBirthDay) {
        this.memberBirthDay = memberBirthDay;
    }

    public String getMemberTeam() {
        return memberTeam;
    }

    public void setMemberTeam(String memberTeam) {
        this.memberTeam = memberTeam;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
    public void input(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập mã thành viên");
        this.setMemberCode(scanner.nextLine());
        System.out.println("Nhập họ tên thành viên");
        this.setMemberName(scanner.nextLine());
        System.out.println("Nhập giới tính thành viên");
        this.setMemberSex(scanner.nextLine());
        System.out.println("Nhập năm sinh thành viên");
        this.setMemberBirthDay(scanner.nextInt());
        System.out.println("Nhập Team");
        this.setMemberTeam(scanner.nextLine());
        System.out.println("Nhập chức vụ");
        this.setPosition(scanner.nextLine());
    }
}
