package J02_Day3_Excercise;

import J02_Day3.Excercise01.Student;

import java.util.Scanner;

public class Main03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student03 student = new Student03();
        student.setInput();
        while (true){
            menu();
            byte choose =scanner.nextByte();
            switch (choose){
                case 1:{
                    upCase(student);
                    break;
                }
                case 2:{
                    pointMax(student);
                    break;
                }
                case 3:{
                    pointMin(student);
                    break;
                }
                case 4:{
                    editPoint(student);
                    break;
                }
                case 5:{
                    return;
                }
                default:{
                    break;
                }
            }
        }
    }
    public static void menu(){
        System.out.println("1. Viết hoa đối tượng sinh viên vừa nhập vào");
        System.out.println("2. In ra màn hình điểm cao nhất của đối tượng");
        System.out.println("3. In ra màn hình điểm thấp nhất của đối tượng");
        System.out.println("4. Sửa điểm thi của một môn bất kì và kiểm tra lại môn nào có điểm cao nhất");
        System.out.println("5. Thoát");
        System.out.println("Mời bạn chọn");
    }
    public static void upCase(Student03 student){
        System.out.println(student.getStudentName().toUpperCase());
    }
    public static void pointMax(Student03 student){
        float max = student.getPointA1();
        if (max < student.getPointA2())
            max = student.getPointA2();
        else if (max < student.getPointA3())
            max = student.getPointA3();
        else if (max < student.getPointBasicComputer())
            max = student.getPointBasicComputer();
        else if (max < student.getPointTechniqueProgram())
            max = student.getPointTechniqueProgram();
        if (max == student.getPointA1())
            System.out.println("Điểm cao nhất là điểm Toán A1 ");
        else if (max ==student.getPointA2())
            System.out.println("Điểm cao nhất là điểm Toán A2 ");
        else if (max == student.getPointA3())
            System.out.println("Điểm cao nhất là điểm Toán A3 ");
        else if (max == student.getPointBasicComputer())
            System.out.println("Điểm cao nhất là điểm Tin học đại cương ");
        else
            System.out.println("Điểm cao nhất là điểm Kỹ Thuật Lập Trình  ");
    }
    public static void pointMin(Student03 student){
        float min = student.getPointA1();
        if (min > student.getPointA2())
            min = student.getPointA2();
        else if (min > student.getPointA3())
            min = student.getPointA3();
        else if (min > student.getPointBasicComputer())
            min = student.getPointBasicComputer();
        else if (min > student.getPointTechniqueProgram())
            min = student.getPointTechniqueProgram();
        if (min == student.getPointA1())
            System.out.println("Điểm thấp nhất là điểm Toán A1 ");
        else if (min ==student.getPointA2())
            System.out.println("Điểm thấp nhất là điểm Toán A2 ");
        else if (min == student.getPointA3())
            System.out.println("Điểm thấp nhất là điểm Toán A3 ");
        else if (min == student.getPointBasicComputer())
            System.out.println("Điểm thấp nhất là điểm Tin học đại cương ");
        else
            System.out.println("Điểm thấp nhất là điểm Kỹ Thuật Lập Trình  ");
    }
    public static void editPoint(Student03 student){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập môn cần sửa điểm : ");
        String subject = scanner.nextLine();
        if (subject.equalsIgnoreCase("Toán A1")){
            System.out.println("Nhập điểm Toán A1 : ");
            student.setPointA1(scanner.nextFloat());
        }
        else if (subject.equalsIgnoreCase("Toán A2")){
            System.out.println("Nhập điểm Toán A2");
            student.setPointA2(scanner.nextFloat());
        }
        else if (subject.equalsIgnoreCase("Toán A3")){
            System.out.println("Nhập điểm Toán A3");
            student.setPointA3(scanner.nextFloat());
        }
        else if (subject.equalsIgnoreCase("Tin học đại cương")){
            System.out.println("Nhập điểm Tin học đại cương");
            student.setPointBasicComputer(scanner.nextFloat());
        }
        else if (subject.equalsIgnoreCase("Kỹ thuật lập trình")){
            System.out.println("Nhập điểm Kỹ thuật lập trình");
            student.setPointTechniqueProgram(scanner.nextFloat());
        }
        pointMax(student);

    }
}
