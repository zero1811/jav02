package J02_Day3_Excercise;

import java.util.Scanner;

public class Student03 {
    String studentCode;
    String studentName;
    String studentCountry;
    float pointA1;
    float pointA2;
    float pointA3;
    float pointBasicComputer;// Điểm Tin học đại cương
    float pointTechniqueProgram;//Điểm Kỹ thuật lập trình

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentCountry() {
        return studentCountry;
    }

    public void setStudentCountry(String studentCountry) {
        this.studentCountry = studentCountry;
    }

    public float getPointA1() {
        return pointA1;
    }

    public void setPointA1(float pointA1) {
        this.pointA1 = pointA1;
    }

    public float getPointA2() {
        return pointA2;
    }

    public void setPointA2(float pointA2) {
        this.pointA2 = pointA2;
    }

    public float getPointA3() {
        return pointA3;
    }

    public void setPointA3(float pointA3) {
        this.pointA3 = pointA3;
    }

    public float getPointBasicComputer() {
        return pointBasicComputer;
    }

    public void setPointBasicComputer(float pointBasicComputer) {
        this.pointBasicComputer = pointBasicComputer;
    }

    public float getPointTechniqueProgram() {
        return pointTechniqueProgram;
    }

    public void setPointTechniqueProgram(float pointTechniqueProgram) {
        this.pointTechniqueProgram = pointTechniqueProgram;
    }
    public void setInput(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập mã sinh viên : ");
        this.setStudentCode(scanner.nextLine());
        System.out.println("Nhập họ tên sinh viên : ");
        this.setStudentName(scanner.nextLine());
        System.out.println("Nhập quê quán của sinh viên : ");
        this.setStudentCountry(scanner.nextLine());
        System.out.println("Nhập điểm Toán A1 : ");
        this.setPointA1(scanner.nextFloat());
        System.out.println("Nhập điểm Toán A2 : ");
        this.setPointA2(scanner.nextFloat());
        System.out.println("Nhập điểm Toán A3 : ");
        this.setPointA3(scanner.nextFloat());
        System.out.println("Nhập điểm Tin học đại cương : ");
        this.setPointBasicComputer(scanner.nextFloat());
        System.out.println("Nhập điểm Kỹ thuật lập trình : ");
        this.setPointTechniqueProgram(scanner.nextFloat());
    }
}
