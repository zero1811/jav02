package J02_Day3_Excercise;

import java.util.Scanner;

public class Product02 {
    String productCode;
    String productName;
    int purchasePrice;
    int price;
    int amount;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(int purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    public void input(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập mã sản phẩm : ");
        this.setProductCode(scanner.nextLine());
        System.out.println("Nhập tên sản phẩm : ");
        this.setProductName(scanner.nextLine());
        System.out.println("Nhập giá mua : ");
        this.setPurchasePrice(scanner.nextInt());
        System.out.println("Nhập giá bán : ");
        this.setPrice(scanner.nextInt());
        System.out.println("Nhập số lượng : ");
        this.setAmount(scanner.nextInt());
    }
    public  int getinterest(int amount){
        return (this.getPrice()*amount)  - (this.getPurchasePrice()*amount);
    }
}
