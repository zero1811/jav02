package Example;

import java.util.Scanner;

public class ArrayExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập x : ");
        int x = scanner.nextInt();
        System.out.println("Nhập y : ");
        int y = scanner.nextInt();
        int [][] array = new int[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                array[i][j] = i*j;
            }
        }
        System.out.println();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(array[i][j] +" ");
            }
            System.out.println();
        }
    }
}
