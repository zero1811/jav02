package Example;

import java.util.Scanner;

public class Example01 {
    public static void main(String[] args) {
        System.out.println("Nhập số điện thoại : ");
        Scanner scanner = new Scanner(System.in);
        String mobile = scanner.nextLine();
        String pattern = "0[0-9]{9,10}";
        if (mobile.matches(pattern))
            System.out.println("Nhập đúng");
        else
            System.out.println("Nhập sai");
    }
}
