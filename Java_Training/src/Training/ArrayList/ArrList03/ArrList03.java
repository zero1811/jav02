package Training.ArrayList.ArrList03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ArrList03 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList <String> cauHoi = new ArrayList<String>();
        for (int i = 0; i < 5; i++) {
            String nhap = scanner.nextLine();
            cauHoi.add(nhap);
        }
        Collections.shuffle(cauHoi);
        for (int i = 0; i < 5; i++) {
            System.out.println(cauHoi.get(i));
        }
    }
}
