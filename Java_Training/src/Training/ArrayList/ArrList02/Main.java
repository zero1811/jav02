package Training.ArrayList.ArrList02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void menu(){
        System.out.println("1. Nhập danh sách sinh viên. ");
        System.out.println("2. Xuất danh sách sinh viên đã nhập. ");
        System.out.println("3. Xuất danh sách sinh viên theo khoảng điểm. ");
        System.out.println("4. Tìm sinh viên theo họ tên. ");
        System.out.println("5. Tìm và sửa sinh viên theo họ tên. ");
        System.out.println("6. Tìm và xóa sinh viên theo họ tên. ");
        System.out.println("7. Kết thúc. ");
        System.out.println("8. Sắp xếp danh sách sinh viên theo điểm. ");
        System.out.println("9. Sắp xếp danh sách sinh viên theo tên. ");
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList <Student> danhSach = new ArrayList<Student>();
        while (true){
            menu();
            int choose = scanner.nextInt();
            switch (choose){
                case 1:{
                    System.out.println("Nhập danh sách sinh viên : ");
                    nhapDanhSach(danhSach);
                    break;
                }
                case 2:{
                    System.out.println("Xuất danh sách sinh viên : ");
                    xuatDanhSach(danhSach);
                    break;
                }
                case 3:{
                    System.out.println("Xuất danh sách theo khoảng điểm : ");
                    System.out.println("Nhập khoảng điểm cần xuất : ");
                    double diemMin = scanner.nextDouble();
                    double diemMax = scanner.nextDouble();
                    xuatDanhSachTheoDiem(danhSach,diemMin,diemMax);
                    break;
                }
                case 4:{
                    System.out.println("Tìm sinh viên theo họ tên : ");
                    timSinhVien(danhSach);
                    break;
                }
                case 5:{
                    System.out.println("Tìm và sửa sinh viên theo họ tên : ");
                    suaSinhVien(danhSach);
                    break;
                }
                case 6:{
                    System.out.println("Tìm và xóa sinh viên theo tên : ");
                    xoaSinhVien(danhSach);
                    break;
                }
                case 7:{
                    return;
                }
                case 8:{
                    System.out.println("Sắp xếp sinh viên theo điểm : ");
                    sapXepSinhVienTheoDiem(danhSach);
                    break;
                }
                case 9:{
                    System.out.println("Sắp xếp danh sách sinh viên theo tên : ");
                    sapXepSinhVienTheoTen(danhSach);
                    break;
                }
            }
        }

    }
    public static void nhapDanhSach(ArrayList <Student> a){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập tổng số sinh viên: ");
        int sv = scanner.nextInt();
        for (int i = 0; i < sv; i++) {
            Student std = new Student();
            std.nhap();
            a.add(std);
        }
    }
    public static void xuatDanhSach(ArrayList <Student> a){
        for (int i = 0; i < a.size(); i++) {
            Student std = a.get(i);
            System.out.printf("|%30s | %2.2f| \n",std.getHoTen(),std.getDiemTB());
        }
    }
    public static void xuatDanhSachTheoDiem(ArrayList<Student> a,double dmin,double dmax){
        for (int i = 0; i < a.size(); i++) {
            Student std = a.get(i);
            if (std.getDiemTB() >= dmin || std.getDiemTB() <= dmax){
                System.out.printf("|%30s | %2.2f| \n",std.getHoTen(),std.getDiemTB());
            }
        }
    }
    public static void timSinhVien(ArrayList <Student> a){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập Họ Tên Sinh Viên Cần Tìm : ");
        String hoTen = scanner.nextLine();
        for (int i = 0; i < a.size(); i++) {
            Student std = a.get(i);
            if (std.getHoTen().equals(hoTen))
                System.out.printf("|%30s | %2.2f| \n",std.getHoTen(),std.getDiemTB());
        }
    }
    public static void suaSinhVien(ArrayList <Student> a){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên sinh viên cần sửa : ");
        String hoTen = scanner.nextLine();
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i).getHoTen().equals(hoTen)){
                Student std = new Student();
                std.nhap();
                a.add(i,std);
                a.remove(i+1);
                break;
            }
        }
    }
    public static void xoaSinhVien(ArrayList <Student> a){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên sinh viên cần xóa : ");
        String hoTen = scanner.nextLine();
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i).getHoTen().equals(hoTen))
                a.remove(i);
        }
    }
    public static void sapXepSinhVienTheoDiem(ArrayList <Student> a){
        Collections.sort(a, new Comparator<Student>() {
            @Override
            public int compare(Student std1, Student std2) {
                if (std1.getDiemTB() < std2.getDiemTB())
                    return -1;
                else {
                    if (std1.getDiemTB() == std2.getDiemTB())
                        return  0;
                    else
                        return 1;
                }
            }
        });
    }
    public static void sapXepSinhVienTheoTen(ArrayList <Student> a){
        Collections.sort(a, new Comparator<Student>() {
            @Override
            public int compare(Student std1, Student std2) {
                return std1.getHoTen().compareTo(std2.getHoTen());
            }
        });
    }
}
