package Training.ArrayList.ArrList02;

import java.util.Scanner;

public class Student {
    public  String hoTen;
    public double diemTB;
    //Setter

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setDiemTB(double diemTB) {
        this.diemTB = diemTB;
    }
    //Getter

    public String getHoTen() {
        return hoTen;
    }

    public double getDiemTB() {
        return diemTB;
    }
    //Nhập
    public void nhap(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Họ Tên : ");
        String hoTen = scanner.nextLine();
        System.out.println("Điểm Trung Bình : ");
        double diem = scanner.nextDouble();
        this.setHoTen(hoTen);
        this.setDiemTB(diem);
    }
}
