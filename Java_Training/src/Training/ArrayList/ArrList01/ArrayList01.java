package Training.ArrayList.ArrList01;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayList01 {
    public static void main(String[] args) {
        double sum = 0;
        ArrayList <Double> arr = new ArrayList<Double>();
        for (int i = 0;i < 5; i++) {
            System.out.println("Phần tử thứ "+ i +" : ");
            Scanner scanner = new Scanner(System.in);
            double number = scanner.nextInt();
            arr.add(number);
        }
        for (Double x: arr) {
            sum+=x;
        }
        System.out.println("Tổng các phần tử của mảng là: "+sum);
    }
}
