package Training.String.Str04;

import java.util.Scanner;

public class TachChuoiThanhMang {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập chuỗi cần tách : ");
        String input = scanner.nextLine();
        String daySo [] = input.split(",");
        for (String so : daySo) {
            Integer x = Integer.parseInt(so);
            if (x % 2 == 0)
                System.out.println(x);
            
        }
    }
}
