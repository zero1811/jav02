package Excercise.StringEX.Ex02;

import java.util.Scanner;

public class SanPham {
    String tenSp;
    String hang;
    double donGia;

    public String getTenSp() {
        return tenSp;
    }

    public void setTenSp(String tenSp) {
        this.tenSp = tenSp;
    }

    public String getHang() {
        return hang;
    }

    public void setHang(String hang) {
        this.hang = hang;
    }

    public double getDonGia() {
        return donGia;
    }

    public void setDonGia(double donGia) {
        this.donGia = donGia;
    }
    public void nhap(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập tên sản phẩm : ");
        String ten = scanner.nextLine();
        this.setTenSp(ten);
        System.out.println("Đơn giá : ");
        double gia = scanner.nextDouble();
        this.setDonGia(gia);
        scanner.nextLine();
        System.out.println("Hãng sản xuất : ");
        String hang = scanner.nextLine();
        this.setHang(hang);
    }
    public void xuat(){
        System.out.println("-----------------------------------");
        System.out.println("Tên sản phẩm : "+this.getTenSp());
        System.out.println("Đơn giá : "+this.getDonGia());
        System.out.println("Hãng sản xuất : "+this.getHang());

    }
}
