package Excercise.StringEX.Ex02;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList <SanPham> danhSach = new ArrayList<SanPham>();
        Scanner scanner = new Scanner(System.in);
        for (int i = 0;  ; i++) {
            SanPham input = new SanPham();
            input.nhap();
            danhSach.add(input);
            System.out.println("Nhập tiếp (Y/N) : ");
            if (scanner.next().equalsIgnoreCase("N"))
                break;
        }
        for (int i = 0; i < danhSach.size(); i++) {
            if (danhSach.get(i).getHang().equalsIgnoreCase("Nokia"))
                danhSach.get(i).xuat();
        }
    }
}
