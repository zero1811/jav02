package Excercise.StringEX.EX03;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Student {
    String name;
    String email;
    String phoneNumber;
    String idCard;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    public void input(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên : ");
        String name = scanner.nextLine();
        this.setName(name);
        System.out.println("Nhập CMND : ");
        String id = scanner.nextLine();
        this.setIdCard(id);
        System.out.println("Nhập số điện thoại : ");
        String phone = scanner.nextLine();
        this.setPhoneNumber(phone);
        System.out.println("Nhập email : ");
        String mail = scanner.nextLine();
        this.setEmail(mail);
    }
}
