package Excercise.StringEX.Ex01;

import java.util.Scanner;

public class Ex01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên : ");
        String hoTen = scanner.nextLine();
        String ho = hoTen.substring(0,hoTen.indexOf(" "));
        String dem = hoTen.substring(hoTen.indexOf(" "),hoTen.lastIndexOf(" "));
        String ten = hoTen.substring(hoTen.lastIndexOf(" "));
        System.out.println("Họ : "+ho.toUpperCase());
        System.out.println("Đệm : "+dem.toUpperCase());
        System.out.println("Tên : "+ten.toUpperCase());
    }
}
