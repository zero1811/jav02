package Excercise.ArrayListEX.Excercise03;

import java.util.Scanner;

public class SanPham {
    String tenSanPham;
    int giaSanPham;

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public int getGiaSanPham() {
        return giaSanPham;
    }

    public void setGiaSanPham(int giaSanPham) {
        this.giaSanPham = giaSanPham;
    }
    public void nhap(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Tên Sản Phẩm : ");
        String tenSP = scanner.nextLine();
        this.setTenSanPham(tenSP);
        System.out.println("Giá sản phẩm : ");
        int gia = scanner.nextInt();
        this.setGiaSanPham(gia);
    }

}
