package Excercise.ArrayListEX.Excercise03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void menu(){
        System.out.println("1. Nhập danh sách");
        System.out.println("2. Sắp xếp danh sách giảm giần");
        System.out.println("3. Xóa sản phẩm");
        System.out.println("4. In trung bình giá các sản phẩm");
    }
    public static void main(String[] args) {
        ArrayList <SanPham> danhSach = new ArrayList<SanPham>();
        Scanner scanner = new Scanner(System.in);
        while (true){
            menu();
            byte chon = scanner.nextByte();
            switch (chon){
                case 1:{
                    System.out.println("Nhập danh sách");
                    nhap(danhSach);
                    break;
                }
                case 2:{
                    System.out.println("Sắp xếp giảm dần");
                    sapXepGiamDan(danhSach);
                    break;
                }
                case 3:{
                    System.out.println("Xóa sản phẩm");
                    xoaSanPham(danhSach);
                    break;
                }
                case 4:{
                    System.out.println("Trung bình giá các sản phẩm");
                    trungBinhGia(danhSach);
                    break;
                }
                default:
                    return;
            }
        }
    }
    public static void nhap(ArrayList <SanPham> ds){
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; ; i++) {
            SanPham sp = new SanPham();
            sp.nhap();
            ds.add(sp);
            System.out.println("Nhập tiếp (Y/N)");
            if (scanner.next().equals("N"))
                break;
        }
    }
    public static void xuat(ArrayList <SanPham> ds){
        for (int i = 0; i < ds.size(); i++) {
            System.out.printf("%20s | %2d \n",ds.get(i).getTenSanPham(),ds.get(i).getGiaSanPham());
        }
    }
    public static void sapXepGiamDan(ArrayList <SanPham> ds){
        Collections.sort(ds, new Comparator<SanPham>() {
            @Override
            public int compare(SanPham sp1, SanPham sp2) {
                if (sp1.giaSanPham > sp2.getGiaSanPham())
                    return 1;
                else {
                    if (sp1.getGiaSanPham() == sp2.getGiaSanPham())
                        return 0;
                    else
                        return -1;
                }
            }
        });
        xuat(ds);
    }
    public static void xoaSanPham(ArrayList <SanPham> ds){
        System.out.println("Nhập tên sản phẩm cần xóa : ");
        Scanner scanner = new Scanner(System.in);
        String tenSp = scanner.nextLine();
        for (int i = 0; i < ds.size(); i++) {
            if (ds.get(i).getTenSanPham().equals(tenSp))
                ds.remove(i);
        }
        xuat(ds);
    }
    public static void trungBinhGia (ArrayList <SanPham> ds){
        float giatb = 0;
        for (int i = 0; i < ds.size(); i++) {
            giatb += ds.get(i).getGiaSanPham();
        }
        System.out.println(giatb/(ds.size()+1));
    }
}
