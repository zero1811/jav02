package Excercise.ArrayListEX.Excercise02;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Ex02 {
    public static void menu(){
        System.out.println("1. Nhập danh sách");
        System.out.println("2. Xuất danh sách");
        System.out.println("3. Xuất danh sách ngẫu nhiên");
        System.out.println("4. Sắp xếp danh sách");
        System.out.println("5. Xóa phần tử trong danh sách");
        System.out.println("6. Kết thúc");
    }
    public static void main(String[] args) {
        ArrayList <String> hoTen = new ArrayList<String>();
        Scanner scanner = new Scanner(System.in);
        while (true){
            menu();
            byte chon = scanner.nextByte();
            switch (chon){
                case 1:{
                    System.out.println("Nhập danh sách");
                    nhap(hoTen);
                    break;
                }
                case 2:{
                    System.out.println("Xuất danh sách");
                    xuat(hoTen);
                    break;
                }
                case 3:{
                    System.out.println("Xuất danh sách ngẫu nhiên");
                    xuatNgauNhien(hoTen);
                    break;
                }
                case 4:{
                    System.out.println("Sắp xếp danh sách");
                    sapXepTangDan(hoTen);
                    break;
                }
                case 5:{
                    System.out.println("Xóa phần tử");
                    xoaPhanTu(hoTen);
                    break;
                }
                case 6:{
                    return;
                }
            }
        }

    }
    public static void nhap(ArrayList <String> hoTen){

        for (int i = 0;  ; i++) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Họ tên : ");
            String ten = scanner.nextLine();
            hoTen.add(ten);
            System.out.println("Nhập tiếp (Y/N) : ");
            if (scanner.next().equals("N"))
                break;
        }
    }
    public static void xuat (ArrayList <String> hoTen){
        for (int i = 0; i < hoTen.size(); i++) {
            System.out.println(hoTen.get(i));
        }
    }
    public static void xuatNgauNhien(ArrayList <String> hoTen){
        Collections.shuffle(hoTen);
        xuat(hoTen);
    }
    public static void sapXepTangDan(ArrayList <String> hoTen){
        Collections.sort(hoTen);
        xuat(hoTen);
    }
    public static void xoaPhanTu (ArrayList <String> hoTen){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập họ tên cần xóa : ");
        String ten = scanner.nextLine();
        for (int i = 0; i < hoTen.size(); i++) {
            if (hoTen.get(i).equals(ten))
                hoTen.remove(i);
        }
    }
}
